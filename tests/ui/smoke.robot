*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${URL}      https://www.google.com/
${HOST}     False
${BROWSER}  Chrome  # Firefox

${TITLE}       Hello World!

*** Test Cases ***
Check H1 Title
    Open Browser  ${URL}  ${BROWSER}  remote_url=${HOST}  options=add_argument("--ignore-certificate-errors")
    Wait Until Page Contains  ${TITLE}
